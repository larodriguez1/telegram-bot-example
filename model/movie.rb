class Movie
  attr_reader :title, :awards

  def initialize(movie_title, movie_awards)
    @title = movie_title
    @awards = movie_awards
  end

  def won_awards?
    @awards.include?('N/A')
  end
end
