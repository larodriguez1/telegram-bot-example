Feature: Movie Awards
    Scenario: user send the message /awards followed by a movie title
        When the user send '/awards Titanic' to the telegram bot
        Then the telegram bot answers with 'Won 11 Oscars. 126 wins & 83 nominations total'

    Scenario: user send the message /awards followed by a movie title without awards
        When the user send '/awards Emoji' to the telegram bot
        Then the telegram bot answers with 'Has no awards'
    
    Scenario: user send the message /awards followed by a movie title with only nominations
        When the user send '/awards Heat' to the telegram bot
        Then the telegram bot answers with '15 nominations'

    Scenario: user send the message /awards followed by a movie title that not exists
        When the user send '/awards Not a movie' to the telegram bot
        Then the telegram bot answers with 'The movie does not exist'

    Scenario: user send the message /awards followed by a few movies titles 
        When the user send '/awards Titanic; Emoji; Heat' to the telegram bot
        Then the telegram bot answers with 'Won 11 Oscars. 126 wins & 83 nominations total'
        And the telegram bot answers with 'Has no awards'
        And the telegram bot answers with '15 nominations'