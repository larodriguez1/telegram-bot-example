require_relative '../../model/movie'
API_KEY = ENV['OMDB_API_KEY']
API_URL = 'https://www.omdbapi.com/'.freeze

class MovieRepository
  def find_movie(movie_name)
    response = Faraday.get(API_URL, { t: movie_name, apikey: API_KEY })
    movie = JSON.parse(response.body)
    Movie.new(movie['Title'], movie['Awards']) if movie_exists(movie)
  end

  protected

  def movie_exists(movie)
    movie.key?('Title') && movie.key?('Awards')
  end
end
