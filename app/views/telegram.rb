class TelegramView
  def awards(movie)
    if movie.nil?
      'The movie does not exist'
    elsif movie.won_awards?
      'Not available'
    else
      movie.awards.to_s
    end
  end
end
