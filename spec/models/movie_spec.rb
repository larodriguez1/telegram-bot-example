describe 'Movie' do
  it 'should have an existing title' do
    movie = Movie.new('Titanic', 'Won 11 Oscars, 126 wins & 83 nominations total')
    expect(movie.title).to eq 'Titanic'
  end

  it 'should have awards' do
    movie = Movie.new('Titanic', 'Won 11 Oscars, 126 wins & 83 nominations total')
    expect(movie.awards).to eq 'Won 11 Oscars, 126 wins & 83 nominations total'
  end
end
