def stub_request_definition(a_title, awards)
  api_omdb_response_body = {
    "Title": a_title,
    "Awards": awards
  }

  stub_request(:get, 'https://www.omdbapi.com/?apikey&t=Titanic')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_omdb_response_body.to_json, headers: {})
end

describe 'MovieRepository' do
  it 'should find by movie title' do
    stub_request_definition('Titanic', 'Won 11 Oscars, 126 wins & 83 nominations total')
    movie = MovieRepository.new.find_movie('Titanic')
    expect(movie.title).to eq 'Titanic'
    expect(movie.awards).to eq 'Won 11 Oscars, 126 wins & 83 nominations total'
  end
end
